from django.urls import path
from .views import index as index_homepage
from .views import experience as experience

urlpatterns = [
    path('', index_homepage, name="index-homepage"),
    path('experience', experience, name="experience"),
]
