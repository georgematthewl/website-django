from django.shortcuts import render


# Create your views here.
def index(request):
    return render(request, 'homepage/index.html')


def experience(request):
    context = {"experience_page": "active"}
    return render(request, 'homepage/fabelio-experiences-page.html', context)
