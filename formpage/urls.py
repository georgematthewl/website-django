from django.urls import path
from .views import activity_form
from .views import activity_view

urlpatterns = [
    path('', activity_form, name="form-page"),
    path('result', activity_view, name="result-page")
]