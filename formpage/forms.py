from django import forms


class ActivityForm(forms.Form):
    activity_name = forms.CharField(label='Activity', max_length=30)
    date = forms.DateField(label='Date', input_formats=['%Y-%m-%d'],
                           widget=forms.DateInput(attrs={'type': 'date'}))
    time = forms.TimeField(label="Time", input_formats=['%H:%M'], widget=forms.TimeInput(attrs={'type': 'time'}))
    location = forms.CharField(label='Location')
    category = forms.CharField(label='Category', max_length=20)
