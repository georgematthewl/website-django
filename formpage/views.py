from django.http import HttpResponseRedirect
from django.shortcuts import render
from .forms import ActivityForm
from .models import Activity


# Create your views here.

def activity_form(request):
    if request.method == "POST":
        form = ActivityForm(request.POST)
        if form.is_valid():
            data = form.cleaned_data
            Activity.objects.create(**data)
            return HttpResponseRedirect('/form/result')
    else:
        form = ActivityForm()
    context = {"form_page": "active", 'form': form}
    return render(request, 'form/activity-form.html', context)


def activity_view(request):
    if request.method == "POST":
        Activity.objects.all().delete()
        return HttpResponseRedirect('/form/result')
    list_activity = list(Activity.objects.all())
    list_activity.reverse()
    context = {'activity': list_activity}
    return render(request, 'form/form-result.html', context)
